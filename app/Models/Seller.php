<?php

namespace App\Models;

use App\Models\Scopes\SellerScope;
use Illuminate\Database\Eloquent\Attributes\ScopedBy;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

#[ScopedBy([SellerScope::class])]
class Seller extends User
{
    use HasFactory;
    protected $table = 'users';

    public function products(): HasMany {
        return $this->hasMany(Product::class);
    }
}
