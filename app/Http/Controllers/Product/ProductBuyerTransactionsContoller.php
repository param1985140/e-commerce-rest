<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Http\Requests\Product\StoreProductBuyerTransactionRequest;
use App\Models\Buyer;
use App\Models\Product;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductBuyerTransactionsContoller extends ApiController
{
    public function store(StoreProductBuyerTransactionRequest $request, Product $product, Buyer $buyer)
    {
        if($buyer->id === $product->seller_id) {
            return $this->errorResponse('The buyer cannot buy the product from himself', 409);
        }

        if(!$product->isAvailable()) {
            return $this->errorResponse('The product is not available', 400);
        }

        if($request->quantity > $product->quantity) {
            return $this->errorResponse('Request quantity is not available at the moment', 400);
        }

        if(!$buyer->isVerified()) {
            return $this->errorResponse('you should be verified to purchase this item', 409);
        }

        if(!$product->seller->isVerified()) {
            return $this->errorResponse('Seller of this product is not verified', 400);
        }

        $transaction = DB::transaction(function () use ($product, $buyer, $request){
            $product->decrement('quantity', $request->quantity);

            $transaction = Transaction::create([
                'quantity' => $request->quantity,
                'product_id' => $product->id,
                'buyer_id' => $buyer->id
            ]);
            return $transaction;
        });

        return $this->showOne($transaction);
    }
}
