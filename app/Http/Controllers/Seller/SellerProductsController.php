<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreSellerProductRequest;
use App\Http\Requests\StoreSellerRequest;
use App\Http\Requests\UpdateSellerProductRequest;
use App\Http\Requests\UpdateSellerRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\Product;
use App\Models\Seller;
use Exception;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

class SellerProductsController extends ApiController
{
    public function index(Seller $seller) {
        $products = $seller->products;
        return $this->showAll($products);
    }

    public function store(StoreSellerProductRequest $request ,Seller $seller) {
        $data = $request->validated();
        $data['status'] = Product::UNAVAILABLE_PRODUCT;
        $data['seller_id'] = $seller->id;
        $product = Product::create($data);
        return $this->showOne($product,201);
    }

    public function update(UpdateSellerProductRequest $request , Seller $seller, Product $product) {
        /** Points to remember
         * we cannot mark the product as available if there is no category associated with it
        */
        $product->fill($request->validated());
        if($request->status) {
            if($product->isAvailable() && $product->categories()->count() === 0) {
                throw new HttpException(409, 'A product must have atleast one category associated with it');
            }
        }

        if($product->isClean()) {
            return $this->errorResponse('Atleast one value must change to update the product',422);
        }
        $product->save();
        return $this->showOne($product);
    }

    public function destroy(Seller $seller, Product $product) {
        $this->validateSeller($seller, $product);
        $product->delete();
        return $this->showOne($product,204);
    }

    public function validateSeller(Seller $seller, Product $product)
    {
        if($seller->id !== $product->seller_id) {
            throw new HttpException(422, 'You are trying to update someone else product!');
        }
    }
}
