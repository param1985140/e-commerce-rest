<?php

use App\Http\Controllers\Buyer\BuyerCategoriesController;
use App\Http\Controllers\Buyer\BuyerCategoriessController;
use App\Http\Controllers\Buyer\BuyerProductsController;
use App\Http\Controllers\Buyer\BuyersController;
use App\Http\Controllers\Buyer\BuyerSellersController;
use App\Http\Controllers\Buyer\BuyerTransactionsController;
use App\Http\Controllers\Category\CategoriesController;
use App\Http\Controllers\Category\CategoryBuyersController;
use App\Http\Controllers\Category\CategoryProductsController;
use App\Http\Controllers\Category\CategorySellersController;
use App\Http\Controllers\Category\CategoryTransactionsContoller;
use App\Http\Controllers\Product\ProductBuyersController;
use App\Http\Controllers\Product\ProductBuyerTransactionsContoller;
use App\Http\Controllers\Product\ProductCategriesController;
use App\Http\Controllers\Product\ProductsController;
use App\Http\Controllers\Product\ProductTransactionsController;
use App\Http\Controllers\Seller\SellerBuyersController;
use App\Http\Controllers\Seller\SellerCategoriesController;
use App\Http\Controllers\Seller\SellerProductsController;
use App\Http\Controllers\Seller\SellersController;
use App\Http\Controllers\Seller\SellerTransactionsController;
use App\Http\Controllers\Transaction\TransactionCategoriesController;
use App\Http\Controllers\Transaction\TransactionsController;
use App\Http\Controllers\Transaction\TransactionSellerController;
use App\Http\Controllers\User\UsersController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:sanctum');

// Route::apiResource([
//     'users' => UsersController::class,
//     'sellers' => SellersController::class,
// ]);

Route::apiResource('users', UsersController::class);
Route::apiResource('sellers', SellersController::class)->only(['index','show']);
Route::apiResource('buyers',BuyersController::class)->only(['index','show']);
Route::apiResource('categories', CategoriesController::class);
Route::apiResource('transactions', TransactionsController::class)->only(['index','show']);
Route::apiResource('products', ProductsController::class)->only(['index','show']);
Route::apiResource('transactions.categories', TransactionCategoriesController::class)->only(['index']);
Route::apiResource('transactions.seller', TransactionSellerController::class)->only(['index']);

//Buyer
Route::apiResource('buyers.transactions', BuyerTransactionsController::class)->only(['index']);
Route::apiResource('buyers.products', BuyerProductsController::class)->only(['index']);
Route::apiResource('buyers.sellers', BuyerSellersController::class)->only(['index']);
Route::apiResource('buyers.categories', BuyerCategoriesController::class)->only(['index']);

//Categories
Route::apiResource('categories.sellers', CategorySellersController::class)->only(['index']);
Route::apiResource('categories.buyers', CategoryBuyersController::class)->only(['index']);
Route::apiResource('categories.products', CategoryProductsController::class)->only(['index']);
Route::apiResource('categories.transactions', CategoryTransactionsContoller::class)->only(['index']);

//Seller
Route::apiResource('sellers.categories', SellerCategoriesController::class)->only(['index']);
Route::apiResource('sellers.transactions', SellerTransactionsController::class)->only(['index']);
Route::apiResource('sellers.buyers', SellerBuyersController::class)->only(['index']);
Route::apiResource('sellers.products', SellerProductsController::class);

//Product
Route::apiResource('products.transactions', ProductTransactionsController::class)->only(['index']);
Route::apiResource('products.buyers', ProductBuyersController::class)->only(['index']);
Route::apiResource('products.categories', ProductCategriesController::class);
Route::apiResource('products.buyer.transactions', ProductBuyerTransactionsContoller::class);
